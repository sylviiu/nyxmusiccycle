const superagent = require(`superagent`);
const cron = require('cron');
const { personalAccessToken } = require('./config.json');
const cp = require('child_process')

const update = () => new Promise((res, rej) => {
    console.log(`Checking for updates...`)
    cp.exec(`git clean -fd`, (err, out, stderr) => {
        if(!err) {
            cp.exec(`git pull`, (err, out, stderr) => {
                if(err) {
                    console.warn(`Unable to pull files!`, err); res()
                } else if(!`${out}`.toLowerCase().includes(`already up to date`)) {
                    console.log(`Updates were made; successfully pulled files -- rebuilding node_modules!`);
                    cp.exec(`npm i`, (e, out, stderr) => {
                        if(!e) {
                            console.log(`Successfully rebuilt node_modules! Restarting...`);
                            process.exit(0);
                        } else {
                            console.error(`Error occurred while rebuilding node_modules: ${e ? e : `-- no output --`}`, e);
                        }
                    })
                } else {
                    console.log(`Up to date!`)
                    res()
                }
            })
        }
    })
});

const randomGen = (num) => {
    let retVal = "";
    let charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var length = 5;
    if(num) {
         length = Math.round(num);
    }
    for (var i = 0, n = charset.length; i < length; ++i) {
         retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}

update().then(async () => {
    

    const run = async () => {
        superagent.get(`https://api.linode.com/v4/linode/instances`).set({ Authorization: `Bearer ${personalAccessToken}` }).then(r => r.body).then(list => {
            const oldMusicApiList = list.data.filter(o => o.label.startsWith(`music`));
            console.log(`${oldMusicApiList.length} music API servers currently running!`);
    
            superagent.get(`https://api.linode.com/v4/images`).set({ Authorization: `Bearer ${personalAccessToken}` }).then(r => r.body.data.filter(d => d[`is_public`] === false && d.label.toLowerCase().includes(`musicapi`))).then(async images => {
                console.log(`Found ${images.length} image(s) -- first result: ${images[0].id} / ${images[0].label}`);

                for(i in oldMusicApiList) {
                    let instance = Number(i)+1;
                    let linode = oldMusicApiList[i];
                    console.log(`starting instance ${instance}, replacing ${linode.id} / ${linode.label}`);

                    superagent.get(`http://${linode.ipv4[0]}:1366/stopSendingPings?auth=GpCYiZoEDRzNAizoAhKk`).then(r => {
                        console.log(`[${linode.id} / ${linode.label}] Successfully sent request for disabling pings! Response:`, r.body)
                    }).catch(e => {
                        if(linode.ipv4[1]) {
                            superagent.get(`http://${linode.ipv4[1]}:1366/stopSendingPings?auth=GpCYiZoEDRzNAizoAhKk`).then(r => {
                                console.log(`[${linode.id} / ${linode.label}] Successfully sent request for disabling pings! Response:`, r.body)
                            }).catch(e => {
                                console.log(`failed to disable ping sending -- prematurely deleting // ${e}`);
                                wait = false;
                            })
                        } else {
                            console.log(`failed to disable ping sending -- prematurely deleting // ${e}`);
                            wait = false;
                        }
                    })

                    setTimeout(async () => {
                        console.log(`Deleting instance of ${linode.id} / ${linode.label}`)
                        try {
                            await superagent.delete(`https://api.linode.com/v4/linode/instances/${linode.id}`).set({ Authorization: `Bearer ${personalAccessToken}` })
                            console.log(`Successfully deleted ${linode.id} / ${linode.label}`)
                        } catch(e) {
                            console.error(`${e}`)
                            try {
                                await superagent.delete(`https://api.linode.com/v4/linode/instances/${linode.id}`).set({ Authorization: `Bearer ${personalAccessToken}` })
                                console.log(`Successfully deleted ${linode.id} / ${linode.label}`)
                            } catch(e) {
                                console.error(`${e}`)
                                try {
                                    await superagent.delete(`https://api.linode.com/v4/linode/instances/${linode.id}`).set({ Authorization: `Bearer ${personalAccessToken}` })
                                    console.log(`Successfully deleted ${linode.id} / ${linode.label}`)
                                } catch(e) {
                                    console.error(`${e}`)
                                }
                            }
                        }; instance++;
                    }, 20000)

                    await new Promise(res => {
                        superagent.post(`https://api.linode.com/v4/linode/instances`).set({ Authorization: `Bearer ${personalAccessToken}` }).send({
                            backups_enabled: false,
                            booted: true,
                            image: `${images[0].id}`,
                            label: `musicApi${randomGen(4)}`,
                            private_ip: true,
                            root_pass: `Diegosg2774512`,
                            region: `us-east`,
                            type: `g6-nanode-1`,
                        }).then(r => r.body).then((inst) => {
                            console.log(`created instance #${instance} // ${inst.id} - ${inst.label}`);

                            let check = setInterval(() => {
                                superagent.get(`https://api.linode.com/v4/linode/instances/${inst.id}`).set({ Authorization: `Bearer ${personalAccessToken}` }).then(r => r.body).then(async i => {
                                    if(i.status.toLowerCase() == `running`) {
                                        console.log(`status of new server is now running!`)
                                        clearInterval(check);
                                        res()
                                    } else console.log(`instance has not switched to "running" yet -- recieved ${i.status}`)
                                }).catch(e => console.error(`${e}`))
                            }, 10000);
                        })
                    })
                };

                console.log(`Successfully cycled servers!`)
            })
        }).catch(e => console.error(`${e}`))
    };

    if(process.argv.find(s => s == `now`)) {
        console.log(`Running now.`)
        run()
    } else {
        setInterval(update, 60000);
        new cron.CronJob(`0 */3 * * *`, run).start();
    }
})